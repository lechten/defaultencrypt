<!--- Local IspellDict: en -->

# Introduction

I’m using [GNU Emacs](https://www.gnu.org/software/emacs/) for almost
everything related to text processing. In particular, for e-mail and
news I’m using
[Gnus](https://www.gnu.org/software/emacs/manual/gnus.html).

On a side note, GNU Emacs comes with built-in support for
[GnuPG](https://www.gnupg.org/), e.g., via the
[EasyPG Assistant](https://www.gnu.org/software/emacs/manual/epa.html),
which is able to en- and decrypt files with the extension `.gpg`
transparently if you put the following into your `~/.emacs`:

    (require 'epa-file)
    (setq epa-file-encrypt-to "<your keyid>")


# DefaultEncrypt for Gnus

Gnus supports GnuPG via the insertion of so-called MML secure tags, which
contain encryption instructions to be performed before a message is sent.
However, by default those tags need to be added manually, which can easily
be forgotten.  In contrast, DefaultEncrypt aims to insert those tags
automatically if public keys for all recipients are available.

I assume that you are familiar with GnuPG support for Gnus messages as
described in [Info node (message) Security](https://www.gnu.org/software/emacs/manual/html_node/message/Security.html).
Moreover, DefaultEncrypt is based on EasyPG, the default interface of
Emacs for OpenPGP and S/MIME, see [Info node (epa)](https://www.gnu.org/software/emacs/manual/html_node/epa/index.html).

DefaultEncrypt aims for automatic insertion of an MML secure encryption
tags into messages if public keys (either GnuPG public keys or S/MIME
certificates) for all recipients are available.  In addition, before a
message is sent, the user is asked whether plaintext should really be sent
unencryptedly when public keys for all recipients are available.

The above is provided by [jl-encrypt.el](./jl-encrypt.el) by adding
`jl-mml-secure-encrypt-if-possible` to `gnus-message-setup-hook` and
`jl-mml-secure-check-encryption-p` to `message-send-hook`.

Note that with EasyPG, e-mail is *not* encrypted to (a key for) the
From address (see [Info node (epa) Mail-mode integration](https://www.gnu.org/software/emacs/manual/html_node/epa/Mail_002dmode-integration.html)).
Instead, to make sure that you can decrypt your own e-mails, typical
options are
* to customize `mml-secure-openpgp-encrypt-to-self` or
* to use a Bcc header to encrypt the e-mail to one of your own
  GnuPG identities (see `mml-secure-safe-bcc-list` then) or
* to use the `encrypt-to` or `hidden-encrypt-to option` in `gpg.conf` or
* to use the `encrypt-to` option in `gpgsm.conf`.

Note that `encrypt-to` gives away your identity for *every*
encryption without warning, which is not what you want if you are
using, e.g., remailers.
Also, be careful with Bcc headers; it *might* be possible to make
this work without giving away the Bcc'ed identities, but it did
not work by default in my case, and I recommend against such a
thing: Only add Bcc if you are absolutely sure that you know what
you are doing.  See also `mml-secure-bcc-is-safe`.

If you are interested in S/MIME then I suggest that you read this
[article on drawbacks of its trust model](https://blogs.fsfe.org/jens.lechtenboerger/2013/12/23/openpgp-and-smime/).
If you are still interested in S/MIME afterwards, take a look
at [jl-smime.el](./jl-smime.el) in addition to
[jl-encrypt.el](./jl-encrypt.el).

## Install
Place [jl-encrypt.el](./jl-encrypt.el) into your load-path and add the
following to `~/.emacs`.

    (require 'jl-encrypt)

If you share my preferences, no further configuration should be necessary.

## Customization
I recommend that you leave the default value of t for
`gnus-message-replyencrypt` unchanged so that your replies to
encrypted messages are automatically encrypted.

Customizable variables defined in this file are
`mml-secure-use-unusable-pgp-keys`,
`mml-secure-insert-signature`, `mml-secure-pgp-without-mime`.

In addition, as explained in the following `mml-default-encrypt-method` and
`mml-default-sign-method` influence whether to prefer S/MIME or GnuPG in
certain situations, and the value of `gnus-message-replysignencrypted` may
be changed by this code.

If `mml-secure-use-unusable-pgp-keys` is t (the default),  MML
secure tags will also be added if only unusable (expired or
revoked) keys are available.  In such a case, encryption will fail
with an error, which allows you to obtain current keys before
sending your e-mail.

If `mml-secure-insert-signature` is nil (the default),
`gnus-message-replysignencrypted` will be set to nil to avoid signatures
for reply messages.  You may manually add MML sign tags any time, of
course, but the whole point of this file is to create MML tags automatically
(e.g., by customizing `mml-secure-insert-signature`).
If GnuPG and S/MIME keys are both available for the given recipients and no
MML tag is present, `mml-default-encrypt-method` determines what method to
prefer.  If `mml-secure-insert-signature` is set to `always`,
`mml-default-sign-method` determines what method to prefer upon message
creation.

## Sanity check
Without any further configuration, send a GnuPG encrypted e-mail to
yourself as follows.  Enter your own e-mail address after To, choose some
Subject, and enter `M-x spook` in the body, which will insert suitable
text.  Then press `C-c C-c` to send the e-mail as usual (forgetting to
encrypt).  If you own a GnuPG public for the To e-mail address then you
will be asked whether you really want to send that e-mail as plaintext.
Answering `no` will insert an MML secure tag for you.  Press `C-c C-c`
again, and an encrypted e-mail will be sent.  If you receive that e-mail
with garbled attachments read the comment for
`mml-secure-pgp-without-mime`.
